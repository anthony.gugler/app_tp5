package ch.heiafr.tic.mobapp.tp05_trivia.model.persistence;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.service.controls.actions.FloatAction;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class StatPreference {
    private static StatPreference INSTANCE = null;
    ;
    private final Application application;
    private final SharedPreferences sharedPref;
    static final String MY_PREFS = "My_preferences";


    private MutableLiveData<Float> rating;//Le pourcentage de réussite du joueur (qc / qr * 100),
    private MutableLiveData<Integer> qr;//Le nombre de questions répondues
    private MutableLiveData<Integer> qc;//Le nombre de questions répondues correctement
    private MutableLiveData<Integer> gameFinish;//nombre partie terminée
    private MutableLiveData<Integer> nbrGold;
    private MutableLiveData<Integer> nbrSilver;
    private MutableLiveData<Integer> nbrBronze;

    private StatPreference(Application application) {
        this.application = application;
        this.sharedPref = application.getApplicationContext().getSharedPreferences(MY_PREFS, Activity.MODE_PRIVATE);
        rating = new MutableLiveData<>(0.0f);
        qr = new MutableLiveData<>(0);
        qc = new MutableLiveData<>(0);
        gameFinish = new MutableLiveData<>(0);
        nbrGold = new MutableLiveData<>(0);
        nbrSilver = new MutableLiveData<>(0);
        nbrBronze = new MutableLiveData<>(0);
    }

    public static StatPreference getInstance(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new StatPreference(application);
        }
        return INSTANCE;
    }

    public LiveData<Float> getRating() {
        return rating;
    }

    public void updateRating() {
        if (qr.getValue() != 0) {
            System.out.println("");
            this.rating.setValue(((float) this.qc.getValue()) / this.qr.getValue() * 100);
            SharedPreferences.Editor editor = this.sharedPref.edit();
            editor.putFloat("rating", this.rating.getValue());
            editor.apply();
        }
    }

    public LiveData<Integer> getQr() {
        return qr;
    }

    public void updateQr(int répondu) {
        this.qr.setValue(this.qr.getValue() + répondu);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt("qr", this.qr.getValue());
        editor.apply();
        updateRating();
    }

    public LiveData<Integer> getQc() {
        return qc;
    }

    public void updateQc( int score) {
        this.qc.setValue(this.qc.getValue() + score);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt("qc", this.qc.getValue());
        editor.apply();
        updateRating();
    }

    public LiveData<Integer> getGameFinish() {
        return gameFinish;
    }

    public void updategameFinish() {
        this.gameFinish.setValue(this.gameFinish.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt("gameFinish", this.gameFinish.getValue());
        editor.apply();
    }

    public LiveData<Integer> getNbrGold() {
        return nbrGold;
    }

    public void updateNbrGold() {
        this.nbrGold.setValue(this.nbrGold.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt("nbrGold", this.nbrGold.getValue());
        editor.apply();
    }

    public LiveData<Integer> getNbrSilver() {
        return nbrSilver;
    }

    public void updateNbrSilver() {
        this.nbrSilver.setValue(this.nbrSilver.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt("nbrSilver", this.nbrSilver.getValue());
        editor.apply();
    }

    public LiveData<Integer> getNbrBronze() {
        return nbrBronze;
    }

    public void updateNbrBronze() {
        this.nbrSilver.setValue(this.nbrSilver.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt("nbrBronze", this.nbrSilver.getValue());
        editor.apply();
    }
}
