package ch.heiafr.tic.mobapp.tp05_trivia.ui.game;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.QuestionRoom;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;

public class GameViewModel extends AndroidViewModel {


    //=== Attributes

    private final TriviaRepository triviaRepository;


    //=== Constructors

    /**
     * Create a GameViewModel object.
     *
     * @param application application context
     */

    public GameViewModel(@NonNull Application application) {
        super(application);

            triviaRepository = TriviaRepository.getInstance(application);
            triviaRepository.fetchQuestion();

    }


    public LiveData<ArrayList<Question>> getQuestion() {

        return triviaRepository.getQuestion();
    }

    public Question getQuestionRoom() {

        return triviaRepository.getQuestionRoom();
    }

    public void setScore(int point) {
        triviaRepository.setScore(point);
    }

    public void addQuestion(QuestionRoom questionRoom) {
        triviaRepository.addQuestionRoom(questionRoom);
    }
}
