package ch.heiafr.tic.mobapp.tp05_trivia.ui.game;

import android.view.View;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;

public interface OnClickAnswerEvents {
    void onAnswerClicked(int index, Question q);
}
