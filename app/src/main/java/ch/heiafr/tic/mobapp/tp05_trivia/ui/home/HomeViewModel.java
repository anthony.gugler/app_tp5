package ch.heiafr.tic.mobapp.tp05_trivia.ui.home;

import android.app.Application;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.model.api.ApiOpenTrivia;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Category;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;

/**
 * ViewModel associated with the result screen
 */
public class HomeViewModel extends AndroidViewModel {

    //=== Attributes

    private final TriviaRepository triviaRepository;


    //=== Constructors

    /**
     * Create a ResultViewModel object.
     *
     * @param application application context
     */
    public HomeViewModel(final Application application) {
        super(application);

        triviaRepository = TriviaRepository.getInstance(application);
//        triviaRepository.determineMedal();

        triviaRepository.fetchCategories();

    }

    //=== Getters and setters
    public void setCategory(Category category) {
        triviaRepository.setCategory(category);
    }

    public LiveData<List<Category>> getCategories() {

        return triviaRepository.getCategories();
    }

}
