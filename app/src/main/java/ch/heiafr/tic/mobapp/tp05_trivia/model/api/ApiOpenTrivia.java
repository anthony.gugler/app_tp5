package ch.heiafr.tic.mobapp.tp05_trivia.model.api;

import android.content.Context;
import android.text.Html;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Category;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;

public class ApiOpenTrivia {
    private Context context;
    private MutableLiveData<List<Category>> resultCategory = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Question>> resultQuestion = new MutableLiveData<ArrayList<Question>>();

    public ApiOpenTrivia(Context context) {
        this.context = context;
    }

    public LiveData<List<Category>> getResultCategory(){
        return resultCategory;
    }

    public LiveData<ArrayList<Question>> getResultQuestion() {
        return resultQuestion;
    }

    public void getCategory() {
        RequestQueue mQueue = Volley.newRequestQueue(context);
        String url = "https://opentdb.com/api_category.php";
        ArrayList<Category> categoryList = new ArrayList<>();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("trivia_categories");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject cat = jsonArray.getJSONObject(i);
                                Category c = new Category(
                                        Integer.valueOf(cat.getString("id")),
                                        cat.getString("name"));
                                categoryList.add(c);
                            }
                            resultCategory.postValue(categoryList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        // Add the request to the RequestQueue.
        mQueue.add(request);


    }

    public void getQuestion(int id, int nbQuestion) {
        RequestQueue mQueue = Volley.newRequestQueue(context);
        String url = "https://opentdb.com/api.php?amount=" + nbQuestion +
                "&category=" + id + "&type=multiple";

        ArrayList<Question> questionsList = new ArrayList<>();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Question q =null;
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject questions = jsonArray.getJSONObject(i);
                                ArrayList<String> answers = new ArrayList<>();
                                JSONArray lesAnswers = questions.getJSONArray("incorrect_answers");
                                for (int j = 0; j < lesAnswers.length(); j++) {
                                    answers.add(Html.fromHtml((lesAnswers.getString(j))).toString());
                                }
                                answers.add(Html.fromHtml(questions.getString("correct_answer")).toString());
                                Collections.shuffle(answers);

                                q = new Question(
                                        Html.fromHtml(questions.getString("category")).toString(),
                                        Html.fromHtml(questions.getString("difficulty")).toString(),
                                        Html.fromHtml(questions.getString("question")).toString(),
                                        Html.fromHtml(questions.getString("correct_answer")).toString(),
                                        answers, -1, false
                                );
                                questionsList.add(q);
                            }
                            resultQuestion.postValue(questionsList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        // Add the request to the RequestQueue.
        mQueue.add(request);
    }

}
