package ch.heiafr.tic.mobapp.tp05_trivia.ui.history;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.Arrays;
import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.QuestionRoom;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;

/**
 * ViewModel associated with the result screen
 */
public class HistoryViewModel extends AndroidViewModel {

    //=== Attributes

    private final TriviaRepository triviaRepository;


    //=== Constructors

    /**
     * Create a ResultViewModel object.
     *
     * @param application application context
     */
    public HistoryViewModel(final Application application) {
        super(application);


        //fetch question
        triviaRepository = TriviaRepository.getInstance(application);
        triviaRepository.fetchAllHistoryQuestions();

    }


    public LiveData<List<QuestionRoom>> getQuestions() {
        return triviaRepository.getQuestions();
    }


    public void setQuestion(QuestionRoom question) {
        triviaRepository.setQuestion(new Question(question.getCategoryTitle(),
                question.getDifficult(), question.getQuestion(), question.getCorrectAnswer(),
                Arrays.asList(question.getAllAnswer().substring(0
                        , question.getAllAnswer().length() - 2).split("-")),
                question.getGivenAns(), question.isCorrect()));
        ;
    }
}
