package ch.heiafr.tic.mobapp.tp05_trivia.model.trivia;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.model.api.ApiOpenTrivia;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Category;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.QuestionRoom;
import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.HistoryDAO;
import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.HistoryDatabase;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.StatPreference;

/**
 * Repository handling the whole trivia logic and holding data
 */
public class TriviaRepository {

    //=== Constants

    public static final int MODE_HISTORY = 0;               // mode when history
    public static final int MODE_GAME = 1;                  // mode when game
    public static final int DEFAULT_CORRECT_ANSWER = 0;     // default correct answer position
    public static final int QUESTION_NUMBER = 3;           // number of questions
    public static final float MEDAL_GOLD = 0.8f;            // gold medal rating
    public static final float MEDAL_SILVER = 0.6f;          // silver medal rating
    public static final float MEDAL_BRONZE = 0.4f;          // bronze medal rating
    public static final int ANSWER_NUMBER = 4;              // number of answers per question
    public static final int QUESTION_INTERVAL = 1500;       // time interval before next question
    public static final String QUESTION_TYPE = "multiple";  // question type
    public static final String EASY = "easy";               // easy difficulty text
    public static final String MEDIUM = "medium";           // medium difficulty text
    private StatPreference stat;


    //=== Attributes

    private final Application application;                  // application context

    private MutableLiveData<TriviaMedal> medal;             // result medal image
    private MutableLiveData<String> textMedal;              // result medal text
    private MutableLiveData<Integer> corrects;              // correctly answered questions


    private ApiOpenTrivia api;

    private HistoryDAO historyDAO;
    private LiveData<List<QuestionRoom>> lesQuestions;


    //=== Singleton

    private static TriviaRepository INSTANCE;       // class unique instance
    private Category category;
    private Question question;
    private int score;

    // Create the unique instance of the TriviaRepository class.
    private TriviaRepository(Application application) {
        this.application = application;
        api = new ApiOpenTrivia(application);

        HistoryDatabase db = HistoryDatabase.getDatabase(application);
        historyDAO = db.HistoryDAO();

        stat = StatPreference.getInstance(application);


    }


    // Get the unique instance of the TriviaRepository class.
    public static TriviaRepository getInstance(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new TriviaRepository(application);
        }
        return INSTANCE;
    }


    //=== Getters and setters

    // Gets the number of correctly answered questions.
    public LiveData<Integer> getCorrects() {
        return corrects;
    }

    // Get the medal image.
    public LiveData<TriviaMedal> getMedal() {
        return medal;
    }

    // Get the text associated to the medal.
    public LiveData<String> getTextMedal() {
        return textMedal;
    }


    //===========categories=======================
    public LiveData<List<Category>> getCategories() {
        return api.getResultCategory();
    }

    public void fetchCategories() {
        api.getCategory();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    //=================questions================
    public LiveData<ArrayList<Question>> getQuestion() {
        return api.getResultQuestion();
    }

    public void fetchQuestion() {
        if (category!=null){
            api.getQuestion(category.getId(), stat.getGameFinish().getValue() == 0 ? 3 : 4);
        }
    }

    public void setQuestion(Question question) {
        this.question = question;
    }



    //=== Public methods

    public void determineMedal() {
        if (corrects != null) {
            TriviaMedal triMedal = TriviaMedal.NONE;
            String text = "LOOOSER";
            switch (score) {
                case 1:
                    triMedal = TriviaMedal.BRONZE;
                    text = "Could do better";
                    stat.updateNbrBronze();
                    break;
                case 2:
                    triMedal = TriviaMedal.SILVER;
                    text = "Almost done";
                    stat.updateNbrSilver();
                    break;
                case 3:
                    triMedal = TriviaMedal.GOLD;
                    text = "Well done !";
                    stat.updateNbrGold();
                    break;
            }
            medal = new MutableLiveData<>(triMedal);
            textMedal = new MutableLiveData<>(text);
        }


    }

    public void setScore(int point) {
        corrects = new MutableLiveData<>(point);
        stat.updategameFinish();
        stat.updateQc(point);
        this.score = point;
    }


    public LiveData<List<QuestionRoom>> getQuestions() {
        return lesQuestions;
    }

    public void fetchAllHistoryQuestions() {
        lesQuestions = historyDAO.getAll();
    }

    public void addQuestionRoom(QuestionRoom questionRoom) {
        HistoryDatabase.databaseWriteExecutor.execute(() -> {
            historyDAO.insert(questionRoom);
        });
    }

    public Question getQuestionRoom() {
        return question;
    }
}
