package ch.heiafr.tic.mobapp.tp05_trivia.ui.game;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.databinding.FragmentGameBinding;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.QuestionRoom;
import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.StatPreference;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;
import ch.heiafr.tic.mobapp.tp05_trivia.ui.home.HomeFragmentDirections;
import ch.heiafr.tic.mobapp.tp05_trivia.ui.result.ResultFragmentDirections;

import static androidx.navigation.fragment.NavHostFragment.findNavController;

/**
 * Fragment handling the game view
 */
public class GameFragment extends Fragment {

    private FragmentGameBinding binding;

    public GameViewModel viewModel;
    private Integer id_category;
    private int nbQestion = 0;
    private ArrayList<Question> lesQuestions;
    private int point = 0;
    private StatPreference stat;
    private int questionR;

    // Tell the system that this fragment has its own menu.
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    // Inflate the layout using DataBinding and keep a binding object reference which is used to directly access the views.
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game, container, false);
        binding.gameRoot.setVisibility(View.VISIBLE);
        return binding.getRoot();

    }

    // Fragment's logic. Bind views to data and handle navigation.
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // creates a unique instance of the ResultViewModel class
        viewModel = new ViewModelProvider(this).get(GameViewModel.class);
        stat = StatPreference.getInstance(getActivity().getApplication());


        // sets data binding to automatically update the views when a LiveData variable of the ViewModel changes.
        /*
         */
        binding.setViewModel(viewModel);
        id_category = GameFragmentArgs.fromBundle(getArguments()).getIdCat();
        if (GameFragmentArgs.fromBundle(getArguments()).getMode() == TriviaRepository.MODE_GAME) {
            viewModel.getQuestion().observe(getViewLifecycleOwner(), new Observer<ArrayList<Question>>() {
                @Override
                public void onChanged(ArrayList<Question> questions) {
                    lesQuestions = questions;
                    binding.categoryTitle.setText(questions.get(nbQestion).getCategoryTitle());
                    setDifficultyColor(questions.get(nbQestion));
                    binding.setQuestion(questions.get(nbQestion++));
                    binding.gameRoot.setVisibility(View.VISIBLE);
                    point = 0;
                    setTitle(1);
                }
            });
            binding.setListener(new OnClickAnswerEvents() {
                @Override
                public void onAnswerClicked(int index, Question q) {
                    String allAnswer = "";
                    for (String answer : q.getAnswers()) {
                        allAnswer += answer + "-";
                    }
                    if (q.getAnswers().get(index).equals(q.getCorrectAnswer())) {
                        // String categoryTitle, String question, String correctAnswer, String allAnswer, boolean correct, String difficult
                        viewModel.addQuestion(new QuestionRoom(q.getCategoryTitle(), q.getQuestion(),
                                q.getCorrectAnswer(), allAnswer, true, q.getDifficulty(), index));
                        point++;
                        stat.updateQr(1);
                        setBackgroundColor(index, Color.GREEN);
                    } else {
                        viewModel.addQuestion(new QuestionRoom(q.getCategoryTitle(), q.getQuestion(),
                                q.getCorrectAnswer(), allAnswer, false, q.getDifficulty(), index));
                        setBackgroundColor(index, Color.RED);
                        setBackgroundColor(q.getAnswers().indexOf(q.getCorrectAnswer()), Color.GREEN);
                        stat.updateQr(1);
                    }
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (nbQestion < lesQuestions.size()) {
                                binding.categoryTitle.setText(lesQuestions.get(nbQestion).getCategoryTitle());
                                setDifficultyColor(lesQuestions.get(nbQestion));
                                binding.setQuestion(lesQuestions.get(nbQestion++));
                                binding.answer1.setBackgroundColor(Color.WHITE);
                                binding.answer2.setBackgroundColor(Color.WHITE);
                                binding.answer3.setBackgroundColor(Color.WHITE);
                                binding.answer4.setBackgroundColor(Color.WHITE);
                                if (lesQuestions.size() == 4) {
                                    int temp = nbQestion - 1;
                                    setTitle(temp);
                                } else {
                                    setTitle(nbQestion);
                                }
                            } else {
                                viewModel.setScore(point);
                                NavDirections action = GameFragmentDirections.actionGameFragmentToResultFragment();
                                Navigation.findNavController(view).navigate(action);
                            }
                        }
                    }, 2000);

                }
            });
        } else {
            binding.answer1.setBackgroundColor(Color.WHITE);
            binding.answer2.setBackgroundColor(Color.WHITE);
            binding.answer3.setBackgroundColor(Color.WHITE);
            binding.answer4.setBackgroundColor(Color.WHITE);
            Question question = viewModel.getQuestionRoom();
            setDifficultyColor(question);
            binding.categoryTitle.setText(question.getCategoryTitle());
            setDifficultyColor(question);
            binding.setQuestion(question);
            if (question.isCorrect()) {
                setBackgroundColor(question.getGivenAnswer(), Color.GREEN);
            } else {
                setBackgroundColor(question.getGivenAnswer(), Color.RED);
                for (int i = 0; i < question.getAnswers().size(); i++) {
                    if (question.getAnswers().get(i).equals(question.getCorrectAnswer())) {
                        setBackgroundColor(i, Color.GREEN);
                    }
                }
            }

        }
    }

    private void setTitle(int question) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Question " + question + "/3");
    }

    private void setBackgroundColor(int index, int color) {
        switch (index) {
            case 0:
                binding.answer1.setBackgroundColor(color);
                break;
            case 1:
                binding.answer2.setBackgroundColor(color);
                break;
            case 2:
                binding.answer3.setBackgroundColor(color);
                break;
            case 3:
                binding.answer4.setBackgroundColor(color);
                break;
        }
    }

    private void setDifficultyColor(Question question) {
        switch (question.getDifficulty()) {
            case "medium":
                binding.difficulty.setBackgroundColor(Color.GREEN);
                break;
            case "hard":
                binding.difficulty.setBackgroundColor(Color.RED);
                break;
            case "easy":
                binding.difficulty.setBackgroundColor(Color.YELLOW);
                break;
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}
