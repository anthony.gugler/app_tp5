package ch.heiafr.tic.mobapp.tp05_trivia.model.persistence;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.QuestionRoom;

@Dao
public interface HistoryDAO {
    @Query("SELECT * FROM table_question")
    LiveData<List<QuestionRoom>> getAll();

    @Insert()
    void insert(QuestionRoom question);


}
