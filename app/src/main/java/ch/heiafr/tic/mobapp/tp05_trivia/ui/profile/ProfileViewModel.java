package ch.heiafr.tic.mobapp.tp05_trivia.ui.profile;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.StatPreference;

public class ProfileViewModel extends AndroidViewModel {

    private final StatPreference statPref;

    public ProfileViewModel(Application application) {
        super(application);
        this.statPref = StatPreference.getInstance(application);
    }

    public LiveData<Integer> getNbrGold() {
        return this.statPref.getNbrGold();
    }

    public LiveData<Integer> getNbrSilver() {
        return this.statPref.getNbrSilver();
    }

    public LiveData<Integer> getNbrBronze() {
        return this.statPref.getNbrBronze();
    }

    public LiveData<Integer> getGameFinish() {
        return this.statPref.getGameFinish();
    }



    public LiveData<Integer> getQr() {
        return this.statPref.getQr();
    }

    public LiveData<Integer> getQc() {
        return this.statPref.getQc();
    }

    public LiveData<Float> getRating() {
        return this.statPref.getRating();
    }


}
