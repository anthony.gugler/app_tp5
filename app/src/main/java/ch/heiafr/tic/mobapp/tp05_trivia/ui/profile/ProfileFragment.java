package ch.heiafr.tic.mobapp.tp05_trivia.ui.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.databinding.FragmentHomeBinding;
import ch.heiafr.tic.mobapp.tp05_trivia.databinding.FragmentProfileBinding;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;
import ch.heiafr.tic.mobapp.tp05_trivia.ui.home.HomeAdapter;
import ch.heiafr.tic.mobapp.tp05_trivia.ui.home.HomeFragmentDirections;
import ch.heiafr.tic.mobapp.tp05_trivia.ui.home.HomeViewModel;

import static androidx.navigation.fragment.NavHostFragment.findNavController;

/**
 * Fragment handling the profile screen
 */
public class ProfileFragment extends Fragment {

    //=== Fragment's override
    private FragmentProfileBinding binding;  // binding object



    // Inflate the layout using DataBinding and keep a binding object reference which is used to directly access the views.
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        return binding.getRoot();

    }

    // Fragment's logic. Bind views to data and handle navigation.
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.binding.setLifecycleOwner(getViewLifecycleOwner());
        this.binding.setViewModel((ProfileViewModel) new ViewModelProvider(this).get(ProfileViewModel.class));


    }

}
