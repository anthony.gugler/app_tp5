package ch.heiafr.tic.mobapp.tp05_trivia.model.data;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


/**
 * Data class to hold data about a question
 */
@Entity(tableName = "table_question")
public class QuestionRoom {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "category")
    private String categoryTitle;// category title
    @ColumnInfo(name = "question")
    private String question;          // question title
    @ColumnInfo(name = "correctAnswer")
    private String correctAnswer;              //correct answer
    @ColumnInfo(name = "allAnswer")
    private String allAnswer;
    @ColumnInfo(name = "correct")
    private boolean correct;
    @ColumnInfo(name = "difficult")
    private String difficult;
    @ColumnInfo(name = "givenAns")
    private int givenAns;

    public QuestionRoom() {
    }
//=== Getters and setters

    @Ignore
    public QuestionRoom( String categoryTitle, String question, String correctAnswer,
                         String allAnswer, boolean correct, String difficult, int givenAns) {
        this.categoryTitle = categoryTitle;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.allAnswer = allAnswer;
        this.correct = correct;
        this.difficult = difficult;
        this.givenAns= givenAns;
    }

    public QuestionRoom(String categoryTitle, String categoryTitle1, String question,
                        String correctAnswer, String allAnswer, boolean b, String difficulty, int givenAns) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAllAnswer() {
        return allAnswer;
    }

    public void setAllAnswer(String allAnswer) {
        this.allAnswer = allAnswer;
    }

    public String getDifficult() {
        return difficult;
    }

    public void setDifficult(String difficult) {
        this.difficult = difficult;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public int getGivenAns() {
        return givenAns;
    }

    public void setGivenAns(int givenAns) {
        this.givenAns = givenAns;
    }
}
