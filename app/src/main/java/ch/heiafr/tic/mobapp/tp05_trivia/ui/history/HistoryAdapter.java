package ch.heiafr.tic.mobapp.tp05_trivia.ui.history;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.databinding.ItemQuestionBinding;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.QuestionRoom;

/**
 * Adapter handling the history screen
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.QuestionViewHolder> {

    //=== Attributes

    private List<QuestionRoom> items;                       // list of items to display
    private final OnHistoryAdapterEvents listener;      // item click listener callback


    // Constructors

    // Create a HistoryAdapter object.
    public HistoryAdapter(List<QuestionRoom> items, OnHistoryAdapterEvents listener) {
        this.items = items;
        this.listener = listener;
    }


    //=== Recyclerview's overrides

    // Inflate the holder of views for an item using data binding.
    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemQuestionBinding binding = ItemQuestionBinding.inflate(
                LayoutInflater.from(parent.getContext())
                , parent, false);
        return new QuestionViewHolder(binding);
    }

    // Bind the data (model) to the views. Called once per item in the list of items.
    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder holder, int position) {
        QuestionRoom question = items.get(position);
        holder.bind(question, listener);
    }

    // Returns the total number of items in the data set held by the adapter.
    @Override
    public int getItemCount() {
        return items.size();
    }


    //=== Public methods

    // Update the RecyclerView when the model (id est items) has changed.
    public void updateItems(List<QuestionRoom> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    //===  ViewHolders

    /**
     * ViewHolder handling the view of a single item
     */
    public static class QuestionViewHolder extends RecyclerView.ViewHolder {
        private static ItemQuestionBinding binding;


        // Create a HistoryViewHolder object using DataBinding
        public QuestionViewHolder(@NonNull ItemQuestionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        // Bind the data to the views using DataBinding. Set content of a list item.
        public void bind(QuestionRoom question, OnHistoryAdapterEvents listener) {
            binding.setQuestion(question);
            binding.setListener(listener);
        }
    }


    //=== Interfaces

    /**
     * Interface handling the events on the list
     */
    public interface OnHistoryAdapterEvents {

        // Called when an item of the list has been clicked.
        void onQuestionClicked(View view, QuestionRoom question);
    }
}
