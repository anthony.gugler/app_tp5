package ch.heiafr.tic.mobapp.tp05_trivia.ui.history;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.databinding.FragmentGameBinding;
import ch.heiafr.tic.mobapp.tp05_trivia.databinding.FragmentHistoryBinding;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.QuestionRoom;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;
import ch.heiafr.tic.mobapp.tp05_trivia.ui.home.HomeFragmentDirections;
import ch.heiafr.tic.mobapp.tp05_trivia.ui.home.HomeViewModel;

/**
 * Fragment handling the history screen
 */
public class HistoryFragment extends Fragment {

    //=== Fragment's overrides
//    private FragmentHistoryBinding binding;
    private FragmentHistoryBinding binding;

    public HistoryViewModel viewModel;

    // Inflate the layout using DataBinding and keep a binding object reference which is used to directly access the views.
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false);
        return binding.getRoot();
    }

    // Fragment's logic. Bind views to data and handle navigation.
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // creates a unique instance of the ResultViewModel class
        viewModel = new ViewModelProvider(this).get(HistoryViewModel.class);
        RecyclerView recyclerView = binding.history;

        // set the adapter and handle click item callback
        final HistoryAdapter adapter = new HistoryAdapter(new ArrayList<>(),
                (view1, questionRoom) -> {
                    viewModel.setQuestion(questionRoom);
                    Navigation.findNavController(view1).
                            navigate(HistoryFragmentDirections.actionHistoryFragmentToGameFragment(TriviaRepository.MODE_HISTORY, -1));

                });
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        viewModel.getQuestions().observe(getViewLifecycleOwner(), questionRooms -> {
            if (questionRooms != null) {
                adapter.updateItems(questionRooms);
            }
        });
    }
}
